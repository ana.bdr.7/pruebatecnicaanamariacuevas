<?php

namespace App\Http\Controllers;

use App\Perro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class PerroController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return response()->json(Perro::all());
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
       
        $validated = \Validator::make($request->all(), [
            'name' => 'required|max:255',
            'tamano' => 'required|max:50',
            'color' => 'required|max:50'
        ]);

        if ($validated->fails())
        {
            return response()->json(["message => ".$validated->errors()]);
        }   
        
        try{
            
            $new = new Perro();
            $new->nombre = $request->name;
            $new->tamano = $request->tamano;
            $new->color = $request->color;
            $new->save();
    
            if($request->file){
    
                $path = Storage::disk('public')->put($new->id,$request->file);
                $new->file = 'http://localhost/pruebaTecnicaAnaMariaCuevas/backend/public/storage/'.$path;
                $new->save();
            }
            return response()->json(['message' => 'Registro añadido correctamente'],200);

        }catch(Exception $e){
            return response()->json(["message => Ha ocurrido un error"],500);
        }
        
                   
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request)
    {   
              
        return response()->json(Perro::find($request->id));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Perro $perro)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Perro $perro)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Perro $perro)
    {
        //
    }

    
}
