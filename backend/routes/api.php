<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PerroController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

//Route::apiResource('perro', PerroController::class);

Route::get('/perro', [PerroController::class, 'index']);

Route::get('/perro/{id}', [PerroController::class, 'show']);

Route::post('/perro', [PerroController::class, 'store']);


